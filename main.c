#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "reverse.h"
#include <string.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <time.h>
#include <errno.h>
//******************************************************************************ERROR
void
error(int err,char *msg)
{
	fprintf(stderr,"%s a retourné %d, message d'erreur : %s\n",msg,err,strerror(errno));
	exit(EXIT_FAILURE);
}
//******************************************************************************END ERROR

//******************************************************************************GLOBAL VARIABLES
int nthreads = 1;
char seed = 'a';
char *fileNames[1];
int fileNumbers = 1;

uint8_t *buffer;
int data_in_buffer = 0;
int max_buffer_size;


pthread_mutex_t mutex;			//mutex for the buffer
pthread_mutex_t mutex2;			//mutex for the right value and global variables
sem_t empty;				//semaphores for the buffer
sem_t full;	

char **result;//suppose that we dont have more than 10 words 
int pos_in_result=0;//cursor into the result tab

//******************************************************************************END OF GLOBAL VARIABLES

//******************************************************************************SOMES METHODS

//******************************************************************************END SOMES METHODS
//******************************************************************************BUFFER OPTIONS

void add_to_buffer(uint8_t *element)
{
	uint8_t *runner = buffer;
	int i = 0;	
	while(i<data_in_buffer && i<max_buffer_size)
	{
		runner++;
		i++;
	}

	runner=element;
	data_in_buffer++;
}

uint8_t *get_into_buffer()
{
	uint8_t *toReturn = (uint8_t *)malloc(sizeof(uint8_t));
	
	int i = 0;
	while(i<data_in_buffer && i<max_buffer_size)
	{
		i++;
	}
	toReturn = buffer+i;
	data_in_buffer--;
	return toReturn;
}
//******************************************************************************END OF BUFFER OPTION


//******************************************************************************PRODUCER FUNCTION


/**The producer function take a filename
   in argument as a char* and put the
   32 bytes results into a buffer for
   the consumme, 1 line = 1 word
 */
void *producer(void *args)
{	
	printf("producer lauched\n");

	//open the file 
	FILE * stream;
	stream = fopen((char *)args ,"rb+");
	if(stream==NULL)
	{
		printf("file not opened");
		return NULL;
	}

	struct stat st;
	stat((char *)args, &st);
	size_t fileSize=st.st_size;
	
	//while here
	//lauch the reverse here ???

	int i = 0;
	uint8_t *send_to_buffer = (uint8_t *)malloc(sizeof(uint8_t));

	while(i<(fileSize/32))
	{
		printf("%d\n",i);
		i++;
	}
	return NULL;

	fread((char *)args,sizeof(uint8_t),fileSize,stream);
	stat((char *)args, &st);
	printf("%s : %lu\n",(char *)args,fileSize);
	for(int i = 0; i < fileSize/32; i+=32)
	{
		
	}
	sem_wait(&empty);
        pthread_mutex_lock(&mutex);
        add_to_buffer(send_to_buffer);
        pthread_mutex_unlock(&mutex);
        sem_post(&full);
	
}


//******************************************************************************END OF PRODUCER FUNCTION



//******************************************************************************CONSUMMER FUNCTION

void *consummer(void *args)
{
	//declare sha
	uint8_t *current = (uint8_t *)malloc(sizeof(uint8_t));
	char *res = (char *)malloc(16*sizeof(char));

	sem_wait(&full);							
	pthread_mutex_lock(&mutex);

	current = get_into_buffer();

	pthread_mutex_unlock(&mutex);
	sem_post(&empty);
	int i;
	if(reversehash(current,res, 16*sizeof(char)))
	{
		for(i=0;*(res+i)!='\0';i++)
		{
			
		}
	}	

	free(current);
	return NULL;	
}

//******************************************************************************END OF CONSUMMER FUNCTION



//******************************************************************************MAIN*********************

int main(int argc, char *argv[])
{	
	//TIMER FOR PERFS TESTS
	clock_t begin = clock();

	
	/**TESTS AND SET OF ARGUMENTS*/
	if(argc<2)
	{
		printf("must have at least 1 argument\n");
		return EXIT_FAILURE;
	}
	int i = 1;
	while(i<argc)
	{
		if(!strcmp(argv[i],"-t"))
		{
			nthreads=atoi(argv[i+1]);
			i+=2;
		}
		if(argv[i]!=NULL &&!strcmp(argv[i],"-l"))
		{
			seed = *argv[i+1];
			i+=2;
		}
		//if -t or -l wasn't present so, here i = 1 cause skip the program's name 
		if(argc-i < 1)
		{
			printf("error no input file name\n");
			return EXIT_FAILURE;
		}

		int j = 0;
		fileNames[argc-i];
		fileNumbers = argc-i;
		while(i<argc)
		{
			fileNames[j]=argv[i];
			j++;
			i++;
		}
		
	}
	/**END ARGUMENTS TESTS AND SET*/

	//need to get the filesize, when it is done, we know the size of the buffer we need
	struct stat st;
	stat(fileNames[0], &st); //only test the first filename
	size_t fileSize=st.st_size;
	result =(char **)malloc(fileSize*sizeof(char *));
	i=0;
	while(i<fileSize)
	{
		*(result+i)=malloc(16*sizeof(char));
	}
	printf("numbers of files : %d\nthreads engaged : %d\nseed : %c\n",fileNumbers,nthreads,seed);
	for(int i = 0; i < fileNumbers; i++)
	{
		printf("%s\n",fileNames[i]);
	}
	void *arg = (void *)fileNames[0];
	producer(arg);
	//here we have all files names and size of each ones
	
	
	//TIMER FOR PERF TESTS
	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("threads : %d, time : %f\n",nthreads,time_spent);
	return EXIT_SUCCESS;
}
