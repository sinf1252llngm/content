Test : matrix.o test.o
	gcc matrix.o test.o -lcunit -g -Wall -W -Werror -std=gnu99 -o test

matrix.o: matrix.c test.c
	gcc -c matrix.c -lcunit -g -Wall -W -Werror -std=gnu99 -o matrix.o

test.o: test.c 
	gcc -c test.c -lcunit -g -Wall -W -Werror -std=gnu99 -o test.o

##############################################################################

.PHONY: clean

clean:
	rm -rf *o
	rm -rf Test
