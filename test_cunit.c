#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include "main.c"

struct stat *st;

size_t *test1;
size_t *test2;
int test_file(void)
{
	st = (struct stat *)malloc(sizeof(struct stat));
	test1 = (size_t *)malloc(sizeof(size_t));
	test2 = (size_t *)malloc(sizeof(size_t));
	stat("01_4c_1k.bin", st);
	*test1=*(st).st_size;
	stat("02_6c_5.bin", st);
	*test2=*(st).st_size;
}

void i_test_file(void)
{
	CU_ASSERT_EQUAL(*test1,32000);
	CU_ASSERT_EQUAL(*test2,5);
}

int clean_test_file(void)
{
	free(st);
	free(test1);
	free(test2);
}
int main() 
{	
/*declaration des registres*/
	CU_pSuite t0 = NULL;
	//CU_pSuite t1 = NULL;
	//CU_pSuite t2 = NULL;
	//CU_pSuite t3 = NULL;
	//CU_pSuite t4 = NULL;
	//CU_pSuite t5 = NULL;
	/*vérification de l'initialisation de suite de tests*/

	if(CUE_SUCCESS != CU_initialize_registry())
	{
		return CU_get_error();
	}

	
	/*Initialisation des suites de tests.(ici int)*/

	t0 = CU_add_suite("test0",test_file,clean_test_file);
	//t1 = CU_add_suite("test1",test_set,clear_test_set);
	//t2 = CU_add_suite("test2",test_get,clear_test_get);
	//t3 = CU_add_suite("test3",test_add,clear_test_add);
	//t4 = CU_add_suite("test4",test_transpose,NULL);
	//t5 = CU_add_suite("test5",test_free,NULL);
	if(NULL == t0 /*||
	   NULL == t1 ||
	   NULL == t2 ||
	   NULL == t3 ||
	   NULL == t4 ||
	   NULL == t5*/)
	{
		CU_cleanup_registry();
		return CU_get_error();
	}

	/*ici void*/
	if((NULL == CU_add_test(t0,"test file",i_test_file)) /*||
	   (NULL == CU_add_test(t1,"test set",i_test_set))	       ||
	   (NULL == CU_add_test(t2,"test get",i_test_get))	       ||
	   (NULL == CU_add_test(t3,"test add",i_test_add))	       ||
	   (NULL == CU_add_test(t4,"test transpose",i_test_transpose)) ||
	   (NULL == CU_add_test(t5,"test free",i_test_free))*/)
 	{
		CU_cleanup_registry();
		return CU_get_error();
	}

	CU_basic_set_mode(CU_BRM_SILENT);
	CU_basic_run_tests();
	CU_cleanup_registry();

	return CU_get_error();
}
